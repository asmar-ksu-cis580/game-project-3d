﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace GameProject3D.Camera
{
    /// <summary>
    /// A camera controlled by WASD + Mouse
    /// </summary>
    public class FpsCamera : ICamera
    {
        // The angle of rotation about the Y-axis
        private float _horizontalAngle = 0;

        // The angle of rotation about the X-axis
        private float _verticalAngle = 0;

        // The camera's position in the world 
        private Vector3 _position;

        // The state of the mouse in the prior frame
        private MouseState _previousMouseState;

        // The Game this camera belongs to
        private Game _game;

        /// <summary>
        /// The view matrix for this camera
        /// </summary>
        public Matrix View { get; protected set; }

        /// <summary>
        /// The projection matrix for this camera
        /// </summary>
        public Matrix Projection { get; protected set; }

        /// <summary>
        /// The sensitivity of the mouse when aiming
        /// </summary>
        public float Sensitivity { get; set; } = 0.0018f;

        /// <summary>
        /// The speed of the player while moving 
        /// </summary>
        public float Speed { get; set; } = 0.5f;

        /// <summary>
        /// Constructs a new FPS Camera
        /// </summary>
        /// <param name="game">The game this camera belongs to</param>
        /// <param name="position">The player's initial position</param>
        public FpsCamera(Game game, Vector3 position)
        {
            _game = game;
            _position = position;

            Projection = Matrix.CreatePerspectiveFieldOfView(
                MathHelper.PiOver4,
                game.GraphicsDevice.Viewport.AspectRatio,
                1,
                1000
            );

            Mouse.SetPosition(game.Window.ClientBounds.Width / 2, game.Window.ClientBounds.Height / 2);
            _previousMouseState = Mouse.GetState();
        }

        /// <summary>
        /// Updates the camera
        /// </summary>
        /// <param name="gameTime">The current GameTime</param>
        public void Update(GameTime gameTime)
        {
            var keyboard = Keyboard.GetState();
            var newMouseState = Mouse.GetState();

            // Get the direction the player is currently facing
            var facing = Vector3.Transform(Vector3.Forward, Matrix.CreateRotationY(_horizontalAngle));

            // Forward and backward movement
            if (keyboard.IsKeyDown(Keys.W)) _position += facing * Speed;
            if (keyboard.IsKeyDown(Keys.S)) _position -= facing * Speed;

            // Strafing movement
            if (keyboard.IsKeyDown(Keys.A)) _position += Vector3.Cross(Vector3.Up, facing) * Speed;
            if (keyboard.IsKeyDown(Keys.D)) _position -= Vector3.Cross(Vector3.Up, facing) * Speed;

            // Adjust horizontal angle
            _horizontalAngle += Sensitivity * (_previousMouseState.X - newMouseState.X);

            // Adjust vertical angle 
            _verticalAngle += Sensitivity * (_previousMouseState.Y - newMouseState.Y);

            var direction = Vector3.Transform(Vector3.Forward,
                Matrix.CreateRotationX(_verticalAngle) * Matrix.CreateRotationY(_horizontalAngle));
            // create the view matrix
            View = Matrix.CreateLookAt(_position, _position + direction, Vector3.Up);

            // Reset mouse state 
            Mouse.SetPosition(_game.Window.ClientBounds.Width / 2, _game.Window.ClientBounds.Height / 2);
            _previousMouseState = Mouse.GetState();
        }
    }
}