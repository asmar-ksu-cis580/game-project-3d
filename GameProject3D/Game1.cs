﻿using System;
using GameProject3D.Camera;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GameProject3D;

public class Game1 : Game
{
    private GraphicsDeviceManager _graphics;
    private SpriteBatch _spriteBatch;
    private SpriteFont _gameFont;

    private Model _cubeModel;
    private Texture2D _cubeTexture;

    private float _previousAngle = -MathHelper.PiOver2;
    private float _angle = 0f;
    private bool _isRotating = false;

    private Random _random = new Random();
    private int _currentReward;
    private int _score = 0;
    private float _indicatorValue = 0f;
    private const int IndicatorWidth = 50;
    private const int IndicatorHeight = 3 * IndicatorWidth;

    public Game1()
    {
        _graphics = new GraphicsDeviceManager(this);
        Content.RootDirectory = "Content";
        IsMouseVisible = true;

        _currentReward = GetNewReward();
    }

    protected override void Initialize()
    {
        base.Initialize();
    }

    protected override void LoadContent()
    {
        _spriteBatch = new SpriteBatch(GraphicsDevice);
        _gameFont = Content.Load<SpriteFont>("gamefont");

        _cubeModel = Content.Load<Model>("cube");
        _cubeTexture = Content.Load<Texture2D>("cube_texture");
    }

    protected override void Update(GameTime gameTime)
    {
        var keyboardState = Keyboard.GetState();
        if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed ||
            keyboardState.IsKeyDown(Keys.Escape))
            Exit();

        if (!_isRotating && keyboardState.IsKeyDown(Keys.Space))
        {
            _isRotating = true;
            _previousAngle = _angle;

            _score += (int)(_indicatorValue * _currentReward);
            _currentReward = GetNewReward();
            _indicatorValue = 0f;
        }

        if (_angle - _previousAngle > MathHelper.PiOver2)
        {
            _isRotating = false;
        }

        switch (_isRotating)
        {
            case true:
            {
                _angle += MathHelper.TwoPi * (float)gameTime.ElapsedGameTime.TotalSeconds;
                if (_angle >= MathHelper.TwoPi)
                {
                    _angle -= MathHelper.TwoPi;
                    _previousAngle -= MathHelper.TwoPi;
                }

                break;
            }
            case false:
                _indicatorValue += (float)gameTime.ElapsedGameTime.TotalSeconds;
                if (_indicatorValue > 1f)
                {
                    _indicatorValue = 0;
                    _isRotating = true;
                    _previousAngle = _angle;
                    _currentReward = GetNewReward();
                }

                break;
        }

        base.Update(gameTime);
    }

    protected override void Draw(GameTime gameTime)
    {
        GraphicsDevice.Clear(Color.CornflowerBlue);

        foreach (var mesh in _cubeModel.Meshes)
        {
            foreach (var effect1 in mesh.Effects)
            {
                var effect = (BasicEffect)effect1;
                effect.EnableDefaultLighting();
                effect.TextureEnabled = true;
                effect.World = Matrix.Identity;
                effect.View = Matrix.CreateRotationY(_angle) * Matrix.CreateLookAt(
                    new Vector3(0, 0, 5),
                    Vector3.Zero,
                    Vector3.Up
                );
                effect.Projection = Matrix.CreatePerspectiveFieldOfView(
                    MathHelper.PiOver4,
                    GraphicsDevice.Viewport.AspectRatio,
                    1,
                    1000
                );
                effect.Texture = _cubeTexture;
                foreach (var pass in effect.CurrentTechnique.Passes)
                {
                    pass.Apply();
                }
            }

            mesh.Draw();
        }

        _spriteBatch.Begin();

        _spriteBatch.DrawString(_gameFont, $"Score: {_score}", new Vector2(32, 32), Color.White);

        if (!_isRotating)
        {
            var viewport = GraphicsDevice.Viewport;
            var viewportDimensions = new Vector2(viewport.Width, viewport.Height);
            var textDimensions = _gameFont.MeasureString($"{_currentReward}");
            var textPosition = (viewportDimensions - textDimensions) / 2;
            _spriteBatch.DrawString(_gameFont, $"{_currentReward}", textPosition, Color.White);

            var indicatorTexture = new Texture2D(GraphicsDevice, 1, 1);
            int indicatorX = viewport.Width * 4 / 5;
            int indicatorY = viewport.Height * 3 / 5;
            indicatorTexture.SetData(new[] { Color.Indigo });
            _spriteBatch.Draw(
                indicatorTexture,
                new Rectangle(
                    indicatorX,
                    indicatorY,
                    IndicatorWidth,
                    4
                ),
                Color.White
            );
            _spriteBatch.Draw(
                indicatorTexture,
                new Rectangle(
                    indicatorX,
                    indicatorY + (int)(IndicatorHeight * (1f - _indicatorValue)),
                    IndicatorWidth,
                    (int)(IndicatorHeight * _indicatorValue)
                ),
                Color.White
            );
        }

        _spriteBatch.End();

        base.Draw(gameTime);
    }

    private int GetNewReward()
    {
        return (int)(_random.NextDouble() * 50) + 1;
    }
}